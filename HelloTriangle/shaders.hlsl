//*********************************************************
//
// Copyright (c) Microsoft. All rights reserved.
// This code is licensed under the MIT License (MIT).
// THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
// IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
// PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.
//
//*********************************************************

//cbuffer SceneConstantBuffer : register(b0)
//{
//    float4x4 matWorldViewProj;
//    float4 padding[12];
//};

cbuffer SceneConstantBuffer  : register(b0)
{
    float4x4 matWorldViewProj;
    float4x4 matWorldView;
    float4x4 matView;
    float4 colMaterial;
    float4 colLight;
    float4 dirLight;
    float4 padding;
};


struct PSInput
{
    float4 position : SV_POSITION;
    float4 norm : NORMAL;
    float4 color : COLOR;
};


PSInput VSMain(float4 position : POSITION, float3 norm : NORMAL, float4 color : COLOR)
{
    PSInput result;

    float4 NW = mul(float4(norm, 0.0f), matWorldView);
    float4 LW = mul(dirLight, matView);
    result.position = mul(position, matWorldViewProj); //mul(float4(position, 1.0f), matWorldViewProj);
    result.color = color;
    //result.color = mul(
    //max(-dot(normalize(LW), normalize(NW)), 0.0f),
    //colLight * color); // lub colMaterial zamiast col
    return result;
}

float4 PSMain(PSInput input) : SV_TARGET
{
    return input.color;
}
